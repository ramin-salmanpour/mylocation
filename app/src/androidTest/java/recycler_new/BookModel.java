package recycler_new;

public class BookModel {

    private String name,auther,isbn,imagrurl;
    private int price ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuther() {
        return auther;
    }

    public void setAuther(String auther) {
        this.auther = auther;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getImagrurl() {
        return imagrurl;
    }

    public void setImagrurl(String imagrurl) {
        this.imagrurl = imagrurl;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "BookModel{" +
                "name='" + name + '\'' +
                ", auther='" + auther + '\'' +
                ", isbn='" + isbn + '\'' +
                ", imagrurl='" + imagrurl + '\'' +
                ", price=" + price +
                '}';
    }
}
